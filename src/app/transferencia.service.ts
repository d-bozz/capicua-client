import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpModule, Http, URLSearchParams, Headers } from '@angular/http';
import 'rxjs/Rx';

@Injectable()
export class TransferenciaService {
  swal:any = require("sweetalert");
  constructor(
    private http: Http,
  )
  {

  }

  ngOnInit()
  {

  }

  transfer(body){
    let promise = new Promise((resolve, reject) => {
      this.http.post('http://localhost:63145/api/Transferencia', body)
        .map((data) => data.json())
        .toPromise()
        .then(
          data => { // Success
                resolve(data);
                this.swal("Buenisimo!.", "La transferencia se ha realizado con éxito.", "success");
          }
        ).catch((ex) => {
          console.log(ex);
          let exception = JSON.parse(ex._body);
          this.swal("Hubo un problema.", exception.ExceptionMessage, "error");

        });
      });
      return promise;
  }

}
