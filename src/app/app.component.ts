import { Component } from '@angular/core';
import {TransferenciaService} from './transferencia.service';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';
  body:any = {};
  transferencia:any = {};

  constructor(
    private TransferenciaService:TransferenciaService
 ) {}


  makeTransfer(body)
  {
    let transferencia = {
      'Contraseña': body.password,
      'CuentaIdOrigen': body.origen,
      'TransferenciaId': body.destino,
      'Monto': body.monto,
      'Descripcion': body.descripcion,
    }
    console.log(transferencia);

    this.TransferenciaService.transfer(transferencia).then(
        resp =>
        {
          this.transferencia = resp;
          console.log(resp);
        }
    );

  }
}
